﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace YuriiKalichak.RobotChallange.UnitTests
{
    public class MovingCalculatorTests
    {
        [Fact]
        public void FindNearestPositionTest_WhenOnePosition_ShoudReturnsExistingPosition()
        {
            //Arrange
            var startPosition = new Position();
            var positionList = new List<Position>
            {
                new Position(1,1)
            };

            //Act
            var result = MovingCalculator.FindNearestPosition(startPosition, positionList);

            //Assert
            Assert.Equal(positionList[0], result);
        }

        [Fact]
        public void FindNearestPositionTest_WhenZeroPositions_ShoudReturnsNull()
        {
            //Arrange
            var startPosition = new Position();
            var positionList = new List<Position>();

            //Act
            var result = MovingCalculator.FindNearestPosition(startPosition, positionList);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void FindNearestPositionTest_WhenValidPositions_ShoudReturnsNearestPostion()
        {
            //Arrange
            var startPosition = new Position();
            var minPos = startPosition.Copy();
            minPos.X += 1;
            var positionList = new List<Position>
            {
                minPos,
                new Position(2,2),
                new Position(1,1),
                new Position(1,5),
                new Position(1,3),
            };

            //Act
            var result = MovingCalculator.FindNearestPosition(startPosition, positionList);

            //Assert
            Assert.Equal(minPos, result);
        }

        [Fact]
        public void CalculateOptimalMovingTest_WhenNoEnergy_ShoudResurnsRobotPosition()
        {
            //Arrange
            var robot = new Robot.Common.Robot
            {
                Energy = 0,
                Position = new Position()
            };
            var finishPosition = new Position(10, 10);

            //Act
            var result = MovingCalculator.CalculateOptimalMoving(robot, finishPosition);

            //Assert
            Assert.Equal(robot.Position, result);
        }

        [Fact]
        public void CalculateOptimalMovingTest_WhenEnergyLowerThanDistance_ShoudResurnsRobotPosition()
        {
            //Arrange
            var robot = new Robot.Common.Robot
            {
                Energy = 5,
                Position = new Position()
            };
            var finishPosition = new Position(10, 10);

            //Act
            var result = MovingCalculator.CalculateOptimalMoving(robot, finishPosition);

            //Assert
            Assert.Equal(robot.Position, result);
        }

        [Fact]
        public void CalculateOptimalMovingTest_WhenEnoughEnergy_ShoudResurnsRobotPosition()
        {
            //Arrange
            var robot = new Robot.Common.Robot
            {
                Energy = 1000,
                Position = new Position()
            };
            var finishPosition = new Position(3, 3);

            //Act
            var result = MovingCalculator.CalculateOptimalMoving(robot, finishPosition);

            //Assert
            Assert.Equal(finishPosition, result);
        }

        [Fact]
        public void GetDistanceTest_WhenDistanceIsNotZero()
        {
            //Arrange
            var startPosition = new Position(0, 0);
            var finishPosition = new Position(5, 0);
            var expected = 5;

            //Act
            var result = MovingCalculator.GetDistanse(startPosition,finishPosition);

            //Assert
            Assert.Equal(expected, result);
        }

        public void GetDistanceTest_WhenDistanceIsZero()
        {
            //Arrange
            var startPosition = new Position(0, 0);
            var finishPosition = new Position(0, 0);

            //Act
            var result = MovingCalculator.GetDistanse(startPosition, finishPosition);

            //Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetEnergyLoss_WhenDistanceIsZero()
        {
            //Arrange
            var startPosition = new Position(0, 0);
            var finishPosition = new Position(0, 0);

            //Act
            var result = MovingCalculator.GetEnegryLoss(startPosition, finishPosition);

            //Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetEnergyLoss_WhenDistanceNotZero()
        {
            //Arrange
            var startPosition = new Position(0, 0);
            var finishPosition = new Position(0, 5);
            var expected = Math.Pow(5,4);
            
            //Act
            var result = MovingCalculator.GetEnegryLoss(startPosition, finishPosition);

            //Assert
            Assert.Equal(expected, result);
        }
    }
}
