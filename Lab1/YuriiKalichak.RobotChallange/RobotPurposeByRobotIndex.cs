﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiKalichak.RobotChallange
{
    public class PurposeByRobotIndex
    {
        public PurposeByRobotIndex(RobotPurpose purpose, int index)
        {
            Purpose = purpose;
            Index = index;
        }

        public RobotPurpose Purpose { get; set; }

        public int Index { get; set; }
    }
}
