﻿namespace YuriiKalichak.RobotChallange
{
    public enum RobotPurpose
    {
        FindNearestStation,
        FindNearestFreeStation,
        AttackEnemy,
        CollectEnergyNearToStation
    }
}
