﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using YuriiKalichak.RobotChallange;

namespace YuriiKalichak.RobotChallange
{
    public class RobotAlgorithm : IRobotAlgorithm
    {
        //Variant constants
        private const int _maxStationEnergy = 1000;
        private const int _maxEnergyGrowth = 100;
        private const int _minEnergyGrowth = 50;
        private const int _energyStationForAttendant = 2;
        private const int _maxEnergyCanCollect = 200;
        private const int _energyLossToCreateNewRobot = 50;
        private const int _attackEnergyLoss = 10;
        private const int _collectingDistance = 3;

        //Algorithm constants
        private const int _energyRemainAfterCreateRobot = _energyLossToCreateNewRobot * 2;

        private int _round = 0;
        private List<PurposeByRobotIndex> _robotPurposes = new List<PurposeByRobotIndex>();

        public RobotAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        public string Author => "Yurii Kalichak";

        public string Description => "Yurii Kalichak's robots";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myRobot = robots[robotToMoveIndex];

            var purposeByRobotIndex = this.GetRobotPurpose(robotToMoveIndex);
            if(purposeByRobotIndex == null)
            {
                purposeByRobotIndex = new PurposeByRobotIndex(RobotPurpose.FindNearestStation, robotToMoveIndex);
                _robotPurposes.Add(new PurposeByRobotIndex(RobotPurpose.FindNearestStation, robotToMoveIndex));
            }

            var stationsPositions = map.Stations.Select(st => st.Position).ToList();
            if (stationsPositions.Contains(myRobot.Position))
            {
                var nearestFreeStation = this.GetNearestFreeStation(myRobot, robots, stationsPositions);
                var nearestStation = this.GetNearestFreeStation(myRobot, robots, stationsPositions);
                var energyCostToStation = MovingCalculator.GetEnegryLoss(myRobot.Position, nearestFreeStation);

                if (nearestFreeStation != null 
                    && myRobot.Energy - _energyRemainAfterCreateRobot > energyCostToStation / (_round * 3) 
                    && _round < 40)
                {
                    purposeByRobotIndex.Purpose = RobotPurpose.FindNearestFreeStation;
                    return new CreateNewRobotCommand
                    {
                        NewRobotEnergy = _energyRemainAfterCreateRobot
                    };
                }
                else if (MovingCalculator.GetDistanse(nearestStation,nearestFreeStation) < MovingCalculator.GetDistanse(myRobot.Position, nearestFreeStation) / 2 
                    && myRobot.Energy - _energyRemainAfterCreateRobot > Math.Sqrt(energyCostToStation))
                {
                    purposeByRobotIndex.Purpose = RobotPurpose.FindNearestStation;
                    return new CreateNewRobotCommand
                    {
                        NewRobotEnergy = _energyRemainAfterCreateRobot
                    };
                }
                return new CollectEnergyCommand();
            }

            if(_round > 45)
            {
                purposeByRobotIndex.Purpose = RobotPurpose.CollectEnergyNearToStation;
            }

            return this.MakeStepAccordingToPurpose(robots, myRobot, map, purposeByRobotIndex);
            
        }

        private RobotCommand MakeStepAccordingToPurpose(
            IList<Robot.Common.Robot> robots, 
            Robot.Common.Robot myRobot, 
            Map map, 
            PurposeByRobotIndex purposeByRobotIndex)
        {
            robots.Remove(myRobot);
            var stationsPositions = map.Stations.Select(st => st.Position).ToList();

            switch (purposeByRobotIndex.Purpose)
            {
                case RobotPurpose.FindNearestFreeStation:
                    {
                        var nearestFree = this.GetNearestFreeStation(myRobot, robots, stationsPositions);
                        if (nearestFree != null)
                        {
                            return new MoveCommand
                            {
                                NewPosition = nearestFree
                            };
                        }
                        break;
                    }
                case RobotPurpose.FindNearestStation:
                    {
                        return this.MoveToNearest(robots, myRobot, stationsPositions);
                    }
                case RobotPurpose.CollectEnergyNearToStation:
                    {
                        var enegryCellsPositions = MovingCalculator.GetEnergyCells(map, 1);
                        var nearestStantionPosition = MovingCalculator.FindNearestPosition(myRobot.Position, enegryCellsPositions);
                        if (myRobot.Position == nearestStantionPosition)
                        {
                            return new CollectEnergyCommand();
                        }
                        return this.MoveToNearest(robots, myRobot, enegryCellsPositions);
                    }
            }
            return this.MoveToNearest(robots, myRobot, stationsPositions);
        }

        private Position GetNearestFreeStation(Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, ICollection<Position> stationPositions)
        {
            var playersOnStations = robots.Where(x => stationPositions.Contains(x.Position)).Select(x=> x.Position);
            var freeStations = stationPositions.Except(playersOnStations);

            return MovingCalculator.FindNearestPosition(robot.Position, freeStations);
        }

        private Position GetNearestStation(Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, ICollection<Position> stationPositions)
        {
            var playersOnStations = robots
                .Where(x => stationPositions.Contains(x.Position) && x.Owner.Name == this.Author)
                .Select(x => x.Position);
            var freeStations = stationPositions.Except(playersOnStations);

            return MovingCalculator.FindNearestPosition(robot.Position, freeStations);
        }

        private PurposeByRobotIndex GetRobotPurpose(int robotIndex)
        {
            return _robotPurposes.FirstOrDefault(x => x.Index == robotIndex);
        }

        private MoveCommand MoveToNearest(IList<Robot.Common.Robot> robots, Robot.Common.Robot myRobot, ICollection<Position> stationsPositions)
        {
            var nearestStantionPosition = MovingCalculator.FindNearestPosition(myRobot.Position, stationsPositions);

            //TODO: friens target recognition
            var playersOnStations = robots.Where(x => stationsPositions.Contains(x.Position));
            var friendsOnStations = playersOnStations.Where(x => x.Owner.Name == this.Author).Select(x => x.Position);
            if (!friendsOnStations.Contains(nearestStantionPosition) || stationsPositions.Count < 5)
            {
                return new MoveCommand
                {
                    NewPosition = MovingCalculator.CalculateOptimalMoving(myRobot, nearestStantionPosition)
                };
            }
            else
            {
                stationsPositions.Remove(nearestStantionPosition);
                return this.MoveToNearest(robots, myRobot, stationsPositions);
            }
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            _round++;
        }
    }
}
