﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YuriiKalichak.RobotChallange
{
    public static class MovingCalculator
    {
        /// <summary>
        ///     Find nearest position to <paramref name="startPosition"/>
        /// </summary>
        /// <param name="startPosition">The position from which the calculation is based</param>
        /// <param name="positions">Collection of positions for finding nearest</param>
        /// <returns>Nearest posiotion</returns>
        public static Position FindNearestPosition(Position startPosition, IEnumerable<Position> positions)
        {
            return positions
                .OrderBy(position => GetDistanse(startPosition, position))
                .FirstOrDefault();
        }

        /// <summary>
        ///     Calculate optionmal position to move. Min steps, max energy
        /// </summary>
        /// <param name="robot">Robot that moving</param>
        /// <param name="finishPosition">Destination position</param>
        /// <returns>Optimal posiotion for getting destination</returns>
        public static Position CalculateOptimalMoving(Robot.Common.Robot robot, Position finishPosition)
        {
            var startPosition = robot.Position;
            var energy = robot.Energy;
            var distance = GetDistanse(robot.Position, finishPosition);
            var maxEnergyСost = GetEnegryLoss(distance);

            if (robot.Energy >= maxEnergyСost)
            {
                return finishPosition;
            }

            var moveDistance = (int)GetMovingDistance(robot, finishPosition);
            var optimalPosition = new Position();

            return new Position
            {
                X = robot.Position.X + (finishPosition.X - robot.Position.X)  * moveDistance / distance,
                Y = robot.Position.Y + (finishPosition.Y - robot.Position.Y) * moveDistance / distance,
            };
        }

        /// <summary>
        ///     Returns energy that will lose after move
        /// </summary>
        /// <param name="position">Start robot position</param>
        /// <param name="finishPosition">Finish position</param>
        /// <returns></returns>
        public static int GetEnegryLoss(Position position, Position finishPosition)
        {
            return GetEnegryLoss(GetDistanse(position, finishPosition));
        }

        /// <summary>
        ///     Returns distanse between two points (<see cref="Position"/>)
        /// </summary>
        /// <param name="position">Firt point</param>
        /// <param name="otherPosition">Second point</param>
        /// <returns></returns>
        public static int GetDistanse(Position position, Position otherPosition)
        {
            return (int)Math.Sqrt(Math.Pow(position.X - otherPosition.X, 2) + Math.Pow(position.Y - otherPosition.Y, 2));
        }

        /// <summary>
        ///  Methot that returns positions where you can collect energy
        /// </summary>
        public static List<Position> GetEnergyCells(Map map, int canCollectEnergyDistance)
        {
            var energyCells = new List<Position>();

            foreach (var station in map.Stations)
            {
                for (int x = map.MinPozition.X; x < map.MaxPozition.X; x++)
                {
                    for (int y = map.MinPozition.Y; y < map.MaxPozition.Y; y++)
                    {
                        if (GetDistanse(station.Position, new Position(x, y)) <= canCollectEnergyDistance)
                        {
                            energyCells.Add(new Position(x, y));
                        }
                    }
                }
            }

            return energyCells;
        }

        /// <summary>
        ///     Returns energy that will lose after move
        /// </summary>
        /// <param name="distance">distance dentween points</param>
        /// <returns></returns>
        private static int GetEnegryLoss(int distance)
        {
            return (int)(Math.Pow(distance, 4));
        }

        private static double GetMovingDistance(Robot.Common.Robot robot, Position finishPosition)
        {
            var distance = GetDistanse(robot.Position, finishPosition);
            var maxDistance = GetDistanse(robot.Position, finishPosition);
            for (; GetEnegryLoss(distance) > robot.Energy / 2; distance--)
            {

                if (robot.Energy - GetEnegryLoss(distance) > maxDistance - distance && robot.Energy > GetEnegryLoss(distance) + 10)
                {
                    break;
                }
            }
            return distance;
        }
    }
}
