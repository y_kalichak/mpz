﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace YuriiKalichak.RobotChallange.Hacked
{
    public static class MethodUtil
    {

        public static void SwapMethodBodies(MethodInfo methodToReplace, MethodInfo methodToInject)
        {
            if (!MethodSignaturesEqual(methodToInject, methodToReplace))
            {
                throw new ArgumentException("a and b must have have same signature");
            }

            RuntimeHelpers.PrepareMethod(methodToReplace.MethodHandle);
            RuntimeHelpers.PrepareMethod(methodToInject.MethodHandle);

            unsafe
            {
                if (IntPtr.Size == 4)
                {
                    int* inj = (int*)methodToInject.MethodHandle.Value.ToPointer() + 2;
                    int* tar = (int*)methodToReplace.MethodHandle.Value.ToPointer() + 2;
//#if DEBUG
//                    Console.WriteLine("\nVersion x86 Debug\n");

//                    byte* injInst = (byte*)*inj;
//                    byte* tarInst = (byte*)*tar;

//                    int* injSrc = (int*)(injInst + 1);
//                    int* tarSrc = (int*)(tarInst + 1);

//                    *tarSrc = (((int)injInst + 5) + *injSrc) - ((int)tarInst + 5);
//#else
                    Console.WriteLine("\nVersion x86 Release\n");
                    *tar = *inj;
//#endif
                }
                else
                {

                    long* inj = (long*)methodToInject.MethodHandle.Value.ToPointer() + 1;
                    long* tar = (long*)methodToReplace.MethodHandle.Value.ToPointer() + 1;
#if DEBUG
                    Console.WriteLine("\nVersion x64 Debug\n");
                    byte* injInst = (byte*)*inj;
                    byte* tarInst = (byte*)*tar;


                    int* injSrc = (int*)(injInst + 1);
                    int* tarSrc = (int*)(tarInst + 1);

                    *tarSrc = (((int)injInst + 5) + *injSrc) - ((int)tarInst + 5);
#else
                    Console.WriteLine("\nVersion x64 Release\n");
                    *tar = *inj;
#endif
                }
            }
        }

    private static bool MethodSignaturesEqual(MethodBase x, MethodBase y)
        {
            if (x.CallingConvention != y.CallingConvention)
            {
                return false;
            }
            Type returnX = GetMethodReturnType(x), returnY = GetMethodReturnType(y);
            if (returnX != returnY)
            {
                return false;
            }
            ParameterInfo[] xParams = x.GetParameters(), yParams = y.GetParameters();
            if (xParams.Length != yParams.Length)
            {
                return false;
            }
            for (int i = 0; i < xParams.Length; i++)
            {
                if (xParams[i].ParameterType != yParams[i].ParameterType)
                {
                    return false;
                }
            }
            return true;
        }
        private static Type GetMethodReturnType(MethodBase method)
        {
            MethodInfo methodInfo = method as MethodInfo;
            if (methodInfo == null)
            {
                // Constructor info.
                throw new ArgumentException("Unsupported MethodBase : " + method.GetType().Name, "method");
            }
            return methodInfo.ReturnType;
        }
        private static bool IsNet20Sp2OrGreater()
        {
            return Environment.Version.Major == FrameworkVersions.Net20SP2.Major &&
                Environment.Version.MinorRevision >= FrameworkVersions.Net20SP2.MinorRevision;
        }
    }
    }
