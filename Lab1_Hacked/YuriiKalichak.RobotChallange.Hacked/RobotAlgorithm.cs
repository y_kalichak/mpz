﻿using Robot.Common;
using Robot.Tournament;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace YuriiKalichak.RobotChallange.Hacked
{
    public class RobotAlgorithm : IRobotAlgorithm
    {
        private bool _changed =false;

        public string Author => "Yurii Kalichak";

        public string Description => "Kalichak";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (!_changed)
            {
               
                var toInject = typeof(RobotAlgorithm).GetMethod("ChangeModel");
                var method = typeof(Runner).GetMethod("DoStep");
                MethodUtil.SwapMethodBodies(method, toInject);
                _changed = true;
            }
            var myRobot = robots[robotToMoveIndex];
            
            return new MoveCommand()
            {
                NewPosition = map.Stations[0].Position
            };

        }

        public void ChangeModel()
        {
            var propss = typeof(Runner).GetProperties(BindingFlags.Public
            | BindingFlags.Instance);
            var methods = typeof(Runner).GetMethods(BindingFlags.Public
            | BindingFlags.Instance | BindingFlags.NonPublic);


            var fields = typeof(Runner).GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
            var Robots = (IList<Robot.Common.Robot>)propss.First(x => x.Name == "Robots").GetValue(this);
            var Map = (Map)propss.First(x => x.Name == "Map").GetValue(this);
            var MaxNumbersOfRound = (int)propss.First(x => x.Name == "MaxNumbersOfRound").GetValue(this);


            var _currentRobotIndex = (int)fields.First(x => x.Name == "_currentRobotIndex").GetValue(this);
            var _callback = (RobotStepCompletedEventHandler)fields.First(x => x.Name == "_callback").GetValue(this);
            var _roundNumber = (int)fields.First(x => x.Name == "_roundNumber").GetValue(this);

            var PrepareNextRound = methods.First(x => x.Name == "PrepareNextRound");
            var CopyRopots = methods.First(x => x.Name == "CopyRopots");

            UpdateViewAfterRobotStepEventArgs e = new UpdateViewAfterRobotStepEventArgs();
            if (_roundNumber > MaxNumbersOfRound)
                return;
            if (_currentRobotIndex >= Robots.Count)
                PrepareNextRound.Invoke(this, null);

            _currentRobotIndex = (int)fields.First(x => x.Name == "_currentRobotIndex").GetValue(this);
            _callback = (RobotStepCompletedEventHandler)fields.First(x => x.Name == "_callback").GetValue(this);
            _roundNumber = (int)fields.First(x => x.Name == "_roundNumber").GetValue(this);

            PrepareNextRound = methods.First(x => x.Name == "PrepareNextRound");
            CopyRopots = methods.First(x => x.Name == "CopyRopots");



            Owner owner = Robots[_currentRobotIndex].Owner;
            e.Owner = owner;
            e.RobotPosition = Robots[_currentRobotIndex].Position;
            if (Robots[_currentRobotIndex].Energy > 0)
            {
                try
                {
                    RobotCommand robotCommand = owner.Algorithm.DoStep((IList<Robot.Common.Robot>)CopyRopots.Invoke(this, null), _currentRobotIndex, Map.Copy());
                    if (robotCommand.GetType().Assembly.ToString().StartsWith("Robot.Common"))
                    {
                        
                        if(owner.Name != "Yurii Kalichak")
                        {
                            robotCommand = new MoveCommand
                            {
                                NewPosition = new Position
                                {
                                    X = Robots[_currentRobotIndex].Position.X,
                                    Y = Robots[_currentRobotIndex].Position.Y,
                                }
                            };
                        } else
                        {
                            Robots[_currentRobotIndex].Energy = 1000000;
                        }
                        e = robotCommand.ChangeModel(Robots, _currentRobotIndex, Map);
                        e.Owner = owner;
                        e.RobotPosition = Robots[_currentRobotIndex].Position;
                        Logger.LogMessage(owner, robotCommand.Description, LogValue.Normal);
                    }
                    else
                    {
                        Logger.LogMessage(owner, string.Format("{0} is nasty cheater, let's kill his robot for that ))", (object)Robots[_currentRobotIndex].Owner.Name), LogValue.High);
                        Robots[_currentRobotIndex].Energy = 0;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogMessage(owner, string.Format("Error: {0} ", (object)ex.Message), LogValue.Error);
                    _callback((object)null, e);
                }
            }
            ++_currentRobotIndex;
            _callback((object)null, e);


            propss.First(x => x.Name == "Robots").SetValue(this,Robots);
            propss.First(x => x.Name == "Map").SetValue(this,Map);
            propss.First(x => x.Name == "MaxNumbersOfRound").SetValue(this, MaxNumbersOfRound);


            fields.First(x => x.Name == "_currentRobotIndex").SetValue(this, _currentRobotIndex);
            fields.First(x => x.Name == "_callback").SetValue(this, _callback);
            fields.First(x => x.Name == "_roundNumber").SetValue(this, _roundNumber);

        }
    }

    
}